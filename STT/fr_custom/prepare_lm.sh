#!/bin/bash

set -xe

if [ ! -f "fr/wiki_fr_lower.txt" ]; then
	curl -sSL https://github.com/Common-Voice/commonvoice-fr/releases/download/lm-0.1/wiki.txt.xz | pixz -d | tr '[:upper:]' '[:lower:]' > fr/wiki_fr_lower.txt
fi;

if [ ! -f "fr/debats-assemblee-nationale.txt" ]; then
	curl -sSL https://github.com/Common-Voice/commonvoice-fr/releases/download/lm-0.1/debats-assemblee-nationale.txt.xz | pixz -d | tr '[:upper:]' '[:lower:]' > fr/debats-assemblee-nationale.txt
fi;

if [ "${ENGLISH_COMPATIBLE}" = "1" ]; then
	mv fr/wiki_fr_lower.txt fr/_wiki_fr_lower_accents.txt
	uni2ascii -q fr/_wiki_fr_lower_accents.txt > fr/wiki_fr_lower.txt

fi;

if [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ] && [ ! -f "fr/excluded_max_sec_lm.txt" ]; then
	cat fr/_*_lm.txt | tr '[:upper:]' '[:lower:]' > fr/excluded_max_sec_lm.txt
	EXCLUDED_LM_SOURCE="fr/excluded_max_sec_lm.txt"
elif [ "${LM_ADD_EXCLUDED_MAX_SEC}" = "1" ] && [ -f "fr/excluded_max_sec_lm.txt" ]; then
	EXCLUDED_LM_SOURCE="fr/excluded_max_sec_lm.txt"
fi;

# Remove special-char <s> that will make KenLM tools choke:
# kenlm/lm/builder/corpus_count.cc:179 in void lm::builder::{anonymous}::ComplainDisallowed(StringPiece, lm::WarningAction&) threw FormatLoadException.
# Special word <s> is not allowed in the corpus.  I plan to support models containing <unk> in the future.  Pass --skip_symbols to convert these symbols to whitespace.
if [ ! -f "sources_lm.txt" ]; then
	cat fr/wiki_fr_lower.txt fr/debats-assemblee-nationale.txt fr/mls_lm.txt ${EXCLUDED_LM_SOURCE} | sed -e 's/<s>/ /g' > fr/sources_lm.txt
fi;