image:
	@cd STT/ && \
	docker build -f Dockerfile.train -t assistant-stt-fr-train:latest . && \
	docker tag assistant-stt-fr-train:latest wasertech/assistant-stt-fr-train:latest && \
	docker push wasertech/assistant-stt-fr-train:latest && \
	echo "All done. Image has be built, tagged and pushed. Congratulation." || \
	echo "Code $?; Something went wrong."